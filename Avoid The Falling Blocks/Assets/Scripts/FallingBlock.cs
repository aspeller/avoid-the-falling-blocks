﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingBlock : MonoBehaviour {

    public Vector2 speedMinMax;
    public float speed;
    float visibleHeightThreshold;

	// Use this for initialization
	void Start () {
        // gradually increase the speed
        speed = Mathf.Lerp(speedMinMax.x, speedMinMax.y, Difficulty.GetDifficultyPercent());

        visibleHeightThreshold = -Camera.main.orthographicSize - transform.localScale.y;

    }
	
	// Update is called once per frame
	void Update () {

        if (Random.Range(0, 1) > 0.5f)
        {
            // blocks are rotated and fall straight down
            transform.Translate(Vector3.down * speed * Time.deltaTime, Space.World);
        }
        else
        {
            // blocks are rotated and fall at an angle
            transform.Translate(Vector3.down * speed * Time.deltaTime, Space.Self);
        }

        if (transform.position.y < visibleHeightThreshold)
        {
            Destroy(gameObject);
        }
    }
}
