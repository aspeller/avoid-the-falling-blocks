﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : MonoBehaviour {

    public float speed = 7;
    float screenHalfWidthWorldUnits;

    public event Action OnPlayerDeath;

	// Use this for initialization
	void Start () {

        float halfPlayerWidth = transform.localScale.x / 2f;

        screenHalfWidthWorldUnits = Camera.main.aspect * Camera.main.orthographicSize + halfPlayerWidth;

    }
	
	// Update is called once per frame
	void Update () {
        float inputX = Input.GetAxisRaw("Horizontal");
        float velocity = inputX * speed;
        transform.Translate(Vector2.right * velocity * Time.deltaTime);

        // if all the way to the left then reappear on the right
        if (transform.position.x < -screenHalfWidthWorldUnits)
        {
            transform.position = new Vector2(screenHalfWidthWorldUnits, transform.position.y);
        }

        // if all the way on the right then reappar on the left
        if (transform.position.x > screenHalfWidthWorldUnits)
        {
            transform.position = new Vector2(-screenHalfWidthWorldUnits, transform.position.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Falling Block")
        {
            if (OnPlayerDeath != null)
            {
                OnPlayerDeath();
            }
            Destroy(gameObject);
        }
    }
}
